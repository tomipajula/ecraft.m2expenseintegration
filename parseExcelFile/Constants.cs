﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace parseExcelFile
{
    class Constants
    {
        public enum Expensestatus
        {
            Draft = 192350000,
            Submitted = 192350001,
            Approved = 192350002,
            Rejected = 192350003,
            Posted = 192350004,
            Paid = 192350005,
        }

    }
}
