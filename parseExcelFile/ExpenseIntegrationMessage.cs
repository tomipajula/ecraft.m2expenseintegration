﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace parseExcelFile
{
    class ExpenseIntegrationMessage
    {
        public DateTime expenseBookingdate { get; set; }
        public string expenseInfo { get; set; }
        public string expenseCategory { get; set; }
        public Guid? expenseCategoryID { get; set; }
        public string expenseProject { get; set; }
        public Guid? expenseProjectID { get; set; }
        public decimal expenseNetto { get; set; }
        public string expenseExpensetype { get; set; }
        public bool Error { get; set; }
        public string Description { get; set; }
    }
}
