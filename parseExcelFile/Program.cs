﻿using System;
using System.Linq;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using AuthenticationUtility;
using Xrm;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Azure.WebJobs.Host;


namespace parseExcelFile
{
    public class Program
    {
        static void Main(string[] args)
        {

            XrmServiceContext crmContext;
            OrganizationServiceProxy service;
            

            //string fileNameOrg = @"C:\Users\tpajula\Documents\Excelfile1.xlsx"; //Tämän haku muistiinpanoista


            try
            {
                
                ClientConfiguration crmConfig = ClientConfiguration.ConnectionString();
                Guid id = new Guid("76d6058a-3360-e911-a983-000d3a296db9");
                // TraceWriter log;
                service = XrmContext.GetXrmProxy(crmConfig);
                crmContext = XrmContext.GetXrmContext(service);
                var note = crmContext.AnnotationSet.FirstOrDefault(x => x.FileName == "newSmallExcelList.xls");
                byte[] imageBytes = Convert.FromBase64String(note.DocumentBody);
                var expense = Parser.Parsing(imageBytes);
                //.Info("6");
                ExpenseIntegration.ExpenseIntegrations(expense, service, crmContext, note.Id); //log);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        private static Cell GetCell(Worksheet worksheet, string columnName, uint rowIndex)
        {
            Row row = GetRow(worksheet, rowIndex);

            if (row == null)
                return null;

            return row.Elements<Cell>().Where(c => string.Compare
                      (c.CellReference.Value, columnName +
                      rowIndex, true) == 0).First();
        }

        // Given a worksheet and a row index, return the row.
        private static Row GetRow(Worksheet worksheet, uint rowIndex)
        {
            return worksheet.GetFirstChild<SheetData>().
                  Elements<Row>().Where(r => r.RowIndex == rowIndex).First();
        }
        public static SharedStringItem GetSharedStringItemById(WorkbookPart workbookPart, int id)
        {
            return workbookPart.SharedStringTablePart.SharedStringTable.Elements<SharedStringItem>().ElementAt(id);
        }
    }
}
