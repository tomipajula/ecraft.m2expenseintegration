﻿using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace parseExcelFile
{
    public class Parser
    {
        //Muuttujat
        private static string Organizationlevel = "Organisaatiotaso 1";
        private static string Personname = "Henkilön nimi";
        private static string IDnumber = "H-Nro";
        private static string Company = "";
        private static string BusinessUnit = "";
        private static string Project = "";
        private static string Customer = "";
        private static string Netto = "Netto";
        private static string Brutto = "Brutto (EUR)";
        private static string Meansofpayment = "Maksuväline";
        private static string Expensetype = "Kululaji";
        private static string Expenseinfo = "Kuluselite";
        private static string Destination = "Matkakohde";
        private static string Travellingday = "Matkalle lähtöpäivä";
        private static string Arrivalday = "Matkalta paluupäivä";
        private static string Traveltype = "Matkatyyppi";
        private static string Nro = "Nro";
        private static string Bookingdate = "Kirjauspäivä";
        private static string Quantity = "Lukumäärä";
        private static string Expensestatus = "Matkalaskun tila";

        public static List<Expense> Parsing(byte[] originalFile)
        {
            List<Expense> expenseList = new List<Expense>();
            List<Error> errorList = new List<Error>();
            MemoryStream stream = new MemoryStream(originalFile);
            //using (var stream = File.Open(originalFileName, FileMode.Open, FileAccess.Read))

            using (stream)
            {
                IExcelDataReader reader;

                reader = ExcelDataReader.ExcelReaderFactory.CreateReader(stream);

                var conf = new ExcelDataSetConfiguration
                {
                    ConfigureDataTable = _ => new ExcelDataTableConfiguration
                    {
                        UseHeaderRow = true,
                        ReadHeaderRow = (rowReader) =>
                        {
                            readrows(rowReader, 14);
                        }
                    }
                };

                var dataSet = reader.AsDataSet(conf);
                var dataTable = dataSet.Tables[0];

                var cellStr = "A15";
                var match = Regex.Match(cellStr, @"(?<col>[A-Z]+)(?<row>\d+)");
                var colStr = match.Groups["col"].ToString();

                var dataSet2 = reader.AsDataSet();
                var dataTable2 = dataSet2.Tables[0];
                var headerRow = dataTable2.Rows[14];


                for (int i = 0; i < dataTable.Rows.Count; i++)
                {
                    Expense exp = new Expense();
                    Error err = new Error();
                    int rowPlacement = i + 1;
                    var currentRow = dataTable.Rows[i];

                    //Organizationlevel
                    exp.Organizationlevel = currentRow[Organizationlevel].ToString();
                    
                    //Personname
                    exp.Personname = currentRow[Personname].ToString();

                    //IDnumber
                    string str = currentRow[IDnumber].ToString();
                    string id = str.Trim();
                    int k;
                    if (int.TryParse(id, out k))
                    {
                        exp.IDnumber = k;
                    }
                    else
                    {
                        err.Cellvalue = id;
                        err.Column = IDnumber;
                        err.Row = rowPlacement;
                        errorList.Add(err);
                    }

                    //Company
                    Company = headerRow.ItemArray.FirstOrDefault(x => x.ToString().StartsWith("Yhtiö")).ToString();
                    exp.Company = currentRow[Company].ToString();

                    //Business Unit
                    BusinessUnit = headerRow.ItemArray.FirstOrDefault(x => x.ToString().StartsWith("Business")).ToString();
                    exp.BusinessUnit = currentRow[BusinessUnit].ToString();

                    //Project
                    Project = headerRow.ItemArray.FirstOrDefault(x => x.ToString().StartsWith("Projekti")).ToString();
                    exp.Project = currentRow[Project].ToString();

                    //Customer
                    Customer = headerRow.ItemArray.FirstOrDefault(x => x.ToString().StartsWith("Asiakas")).ToString();
                    exp.Customer = currentRow[Customer].ToString();

                    //Netto
                    decimal nettous;
                    string netto = currentRow[Netto].ToString();
                    if (decimal.TryParse(netto, out nettous))
                    {
                        exp.Netto = nettous;
                    }
                    else
                    {
                        err.Cellvalue = netto;
                        err.Column = Netto;
                        err.Row = rowPlacement;
                        errorList.Add(err);
                    }

                    //Brutto
                    decimal bruttous;
                    string brutto = currentRow[Brutto].ToString();
                    if (decimal.TryParse(brutto, out bruttous))
                    {
                        exp.Brutto = bruttous;
                    }
                    else
                    {
                        err.Cellvalue = brutto;
                        err.Column = Brutto;
                        err.Row = rowPlacement;
                        errorList.Add(err);
                    }

                    //Means of payment
                    exp.Meansofpayment = currentRow[Meansofpayment].ToString();

                    //Expensetype
                    exp.Expensetype = currentRow[Expensetype].ToString();

                    //Expenseinfo
                    exp.Expenseinfo = currentRow[Expenseinfo].ToString();

                    //Destination
                    exp.Destination = currentRow[Destination].ToString();

                    //Travellingday
                    DateTime travellingdays;
                    string travellingday = currentRow[Travellingday].ToString();
                    if (DateTime.TryParse(travellingday, out travellingdays))
                    {
                        exp.Travellingday = travellingdays;
                    }
                    else
                    {
                        err.Cellvalue = travellingday;
                        err.Column = Travellingday;
                        err.Row = rowPlacement;
                        errorList.Add(err);
                    }

                    //Arrivalday
                    DateTime arrivaldays;
                    string arrivalday = currentRow[Arrivalday].ToString();
                    if (DateTime.TryParse(arrivalday, out arrivaldays))
                    {
                        exp.Arrivalday = arrivaldays;
                    }
                    else
                    {
                        err.Cellvalue = arrivalday;
                        err.Column = Arrivalday;
                        err.Row = rowPlacement;
                        errorList.Add(err);
                    }
                        
                    //Traveltype
                    exp.Traveltype = currentRow[Traveltype].ToString();

                    //Nro
                    string stri = currentRow[Nro].ToString();
                    int j;
                    if (int.TryParse(stri, out j))
                    {
                        exp.Nro = j;
                    }
                    else
                    {
                        err.Cellvalue = stri;
                        err.Column = Nro;
                        err.Row = rowPlacement;
                        errorList.Add(err);
                    }

                    //Bookingdate
                    DateTime bookingdates;
                    string bookingdate = currentRow[Bookingdate].ToString();
                    if (DateTime.TryParse(bookingdate, out bookingdates))
                    {
                        exp.Bookingdate = bookingdates;
                    }
                    else
                    {
                        //bookingdate = "empty";
                        Console.WriteLine("Bookingdate empty");
                        //err.Cellvalue = bookingdate;
                        //err.Column = Bookingdate;
                        //err.Row = rowPlacement;
                        //errorList.Add(err);
                    }

                    //Quantity
                    decimal quantities;
                    string quantity = currentRow[Quantity].ToString();
                    if (decimal.TryParse(quantity, out quantities))
                    {
                        exp.Quantity = quantities;
                    }
                    else
                    {
                        err.Cellvalue = quantity;
                        err.Column = Quantity;
                        err.Row = rowPlacement;
                        errorList.Add(err);
                    }
                        
                    //Expensestatus
                    exp.Expensestatus = currentRow[Expensestatus].ToString();


                    expenseList.Add(exp);
                }
            }
            return expenseList;
        }

        private static void readrows(IExcelDataReader rowReader, int headerRowIndex)
        {
            for (int p = 0; p < headerRowIndex; p++)
                rowReader.Read();
        }
    }
}
