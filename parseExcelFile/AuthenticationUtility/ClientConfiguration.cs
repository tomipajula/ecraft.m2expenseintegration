﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthenticationUtility
{
    public class ClientConfiguration
    {
        public string UriString { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public static ClientConfiguration ConnectionString()
        {

             string connectionString = Environment.GetEnvironmentVariable("EcraftCRM:ConnectionString");
            Debug.WriteLine(connectionString);

            Dictionary<string, string> ConnStringParts = connectionString.Split(';')
                .Where(t => !String.IsNullOrWhiteSpace(t))
                .Select(t => t.Split(new char[] { '=' }, 2))
                .ToDictionary(t => t[0].Trim(), t => t[1].Trim(), StringComparer.InvariantCultureIgnoreCase);

            Debug.WriteLine(ConnStringParts["UriString"]);
            Debug.WriteLine(ConnStringParts["Username"]);
            Debug.WriteLine(ConnStringParts["Password"]);


            return new ClientConfiguration()
            {
                UriString = ConnStringParts["UriString"],
                UserName = ConnStringParts["Username"],
                Password = ConnStringParts["Password"]
            };


        }
    }
}
