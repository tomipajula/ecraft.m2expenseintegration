﻿using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AuthenticationUtility
{
    public class OAuthHelper
    {
        /// <summary>
        /// The header to use for OAuth.
        /// </summary>
        public const string OAuthHeader = "Authorization";

        /// <summary>
        /// Retrieves an authentication header from the service.
        /// </summary>
        /// <returns>The authentication header for the Web API call.</returns>
        /*
        public static string GetAuthenticationHeader(ClientConfiguration cc, bool askConsent = false)
        {
            // Create and get JWT token
            return GetAuthenticationResult(cc, askConsent).CreateAuthorizationHeader();
        }

        public static string GetAuthenticationToken(ClientConfiguration cc, bool askConsent = false)
        {
            // Return access token
            return GetAuthenticationResult(cc, askConsent).AccessToken;
        }

        private static AuthenticationResult GetAuthenticationResult(ClientConfiguration cc, bool askConsent = false)
        {
            string aadTenant = cc.ActiveDirectoryTenant;
            string aadClientAppId = cc.ActiveDirectoryClientAppId;
            string aadResource = cc.ActiveDirectoryResource;

            AuthenticationContext authenticationContext = new AuthenticationContext(aadTenant);

            // OAuth through username and password.
            string username = cc.UserName;
            string password = cc.Password;

            // Get token object
            AuthenticationResult authenticationResult;
            if (askConsent)
            {
                authenticationResult = authenticationContext.AcquireTokenAsync(aadResource, aadClientAppId, new Uri("http://NewAssiIntegrations"), new PlatformParameters(PromptBehavior.Always)).Result;
            }
            else
            {
                var userCredential = new UserPasswordCredential(username, password);
                authenticationResult = authenticationContext.AcquireTokenAsync(aadResource, aadClientAppId, userCredential).Result;
            }
            return authenticationResult;
        }
        */
    }
}
