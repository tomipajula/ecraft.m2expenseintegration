﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace parseExcelFile
{
    public class Error
    {
        public string Column { get; set; }
        public string Cellvalue { get; set; }
        public int Row { get; set; }
    }
}
