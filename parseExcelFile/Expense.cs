﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace parseExcelFile
{
    public class Expense
    {
        public string Organizationlevel { get; set; }
        public int IDnumber { get; set; }
        public string Personname { get; set; }
        public string Company { get; set; }
        public string BusinessUnit { get; set; }
        public string Project { get; set; }
        public string Customer { get; set; }
        public decimal Netto { get; set; }
        public decimal Brutto { get; set; }
        public string Meansofpayment { get; set; }
        public string Expensetype { get; set; }
        public string Expenseinfo { get; set; }
        public string Destination { get; set; }
        public DateTime Travellingday { get; set; }
        public DateTime Arrivalday { get; set; }
        public string Traveltype { get; set; }
        public int Nro { get; set; }
        public DateTime Bookingdate { get; set; }
        public decimal Quantity { get; set; }
        public string Expensestatus { get; set; }
    }
    
}
