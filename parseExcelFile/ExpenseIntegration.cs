﻿using Microsoft.Azure.WebJobs.Host;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xrm;


namespace parseExcelFile
{
    public class ExpenseIntegration
    {
        public static void ExpenseIntegrations(List<Expense> expenselist, OrganizationServiceProxy service, XrmServiceContext crmService, Guid id)//, TraceWriter log) //Try-Catch logittamaan erroreita.
        {
            List<string> Exceptionlist = new List<string>();
            List<ExpenseIntegrationMessage> IntegrationErrorList = new List<ExpenseIntegrationMessage>();
            try
            {
               //log.Info("5");
                foreach (var expense in expenselist)
                {
                    
                    using (var crm = new XrmServiceContext(service)) 
                    {
                        msdyn_project project = crm.msdyn_projectSet.FirstOrDefault(x => x.msdyn_subject == expense.Project);
                        msdyn_expensecategory cat = crm.msdyn_expensecategorySet.FirstOrDefault(x => x.msdyn_name == expense.Expensetype);

                        var nullCat = "";
                        var nullProject = "";
                        Guid? catID;
                        Guid? projectID;
                        if(cat == null)
                        {
                            nullCat = "Category not found";
                            catID = null;
                        }
                        else
                        {
                            nullCat = expense.Expensetype;
                            catID = cat.Id;

                        }
                        
                        if(project == null)
                        {
                            nullProject = "Project not found";
                            projectID = null;
                        }
                        else
                        {
                            nullProject = expense.Project;
                            projectID = project.Id;
                        }
                        try
                        {
                            var crm_expense = new msdyn_expense();

                            crm_expense.msdyn_TransactionDate = expense.Bookingdate;
                            crm_expense.msdyn_name = expense.Expensetype;
                            crm_expense.msdyn_Amount = new Microsoft.Xrm.Sdk.Money(expense.Netto);
                            crm_expense.msdyn_Salestaxamount = new Microsoft.Xrm.Sdk.Money(0);
                            crm_expense.msdyn_Project = new EntityReference(msdyn_project.EntityLogicalName, project.Id);
                            crm_expense.msdyn_ExpenseCategory = new EntityReference(msdyn_expensecategory.EntityLogicalName, cat.Id);
                            crm_expense.msdyn_externaldescription = expense.Expenseinfo;
                            crm_expense.msdyn_ExpenseStatus = new OptionSetValue(192350000);// expense.Expensestatus == "Maksettu" ? new OptionSetValue((int)Constants.Expensestatus.Paid) : expense.Expensestatus == "Hyväksytty (lopullinen)" ? new OptionSetValue((int)Constants.Expensestatus.Posted) : null,                        
                            
                            service.Create(crm_expense);
                        }
                        catch(Exception e)
                        {
                            string line = ErrorParser.Returnerror(e);
                            ExpenseIntegrationMessage expmessage = new ExpenseIntegrationMessage();
                            expmessage.expenseBookingdate = expense.Bookingdate;
                            expmessage.expenseExpensetype = expense.Expensetype;
                            expmessage.expenseNetto = expense.Netto;
                            expmessage.expenseProject = nullProject;
                            expmessage.expenseProjectID = projectID;
                            expmessage.expenseCategoryID = catID;
                            expmessage.expenseCategory = nullCat;
                            expmessage.expenseInfo = expense.Expenseinfo;
                            expmessage.Description = line;
                            expmessage.Error = true;

                            IntegrationErrorList.Add(expmessage);
                            continue;
                        }
                    }
                }

                using (var crm = new XrmServiceContext(service))
                {
                    //Haku
                    var annotation = crm.AnnotationSet.FirstOrDefault(x => x.Id == id);
                    var errorjsonlist = "";
                    for (int i = 0; i < IntegrationErrorList.Count(); i++)
                    {
                        string errorjson = JsonConvert.SerializeObject(IntegrationErrorList[i], Formatting.Indented);
                        errorjsonlist = errorjsonlist + errorjson;
                        
                    }
                    Annotation update = new Annotation();
                    update.Id = annotation.Id;
                    update.NoteText = errorjsonlist;
                    service.Update(update);
                }
            }
            catch (Exception ex)
            {
                //log.Info($"error {ex.ToString()}");
                Exceptionlist.Add(ex.ToString());
            }
        }
    }
}
