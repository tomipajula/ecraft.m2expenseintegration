﻿using AuthenticationUtility;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.WebServiceClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;

namespace Xrm
{
    public class XrmContext
    {
        public static OrganizationServiceProxy GetXrmProxy(ClientConfiguration cc)
        {
            var credentials = new ClientCredentials();
            credentials.UserName.UserName = cc.UserName;
            credentials.UserName.Password = cc.Password;

            var organizationUri = new Uri(cc.UriString);

            OrganizationServiceProxy _serviceProxy = new OrganizationServiceProxy(organizationUri, null, credentials, null);
            _serviceProxy.EnableProxyTypes();

            return _serviceProxy;
        }

        public static XrmServiceContext GetXrmContext(OrganizationServiceProxy proxy)
        {           
            return new XrmServiceContext(proxy);
        }
    }

    
}
