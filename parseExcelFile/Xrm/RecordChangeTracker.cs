﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xrm
{
    public class RecordChangeTracker<E> : IDisposable where E : Entity, System.ComponentModel.INotifyPropertyChanged, new()
    {
        private E origRecord;
        public E RecordWithOnlyChanges { private set; get; }

        public bool hasChanges { private set; get; }

        public string debugInfo = "";

        public RecordChangeTracker(E record)
        {
            origRecord = new E();
            RecordWithOnlyChanges = new E();

            RecordWithOnlyChanges.Id = record.Id;

            record.Attributes.Keys.ToList().ForEach(attrKey =>
            {
                var attrs = record.Attributes.Where(a => a.Key == attrKey);
                var attr = attrs.FirstOrDefault();
                if (!attrs.Any() || attr.Value == null)
                {
                    // Should not happen in CRM, but seems to sometimes appear null values here
                    debugInfo += "Skipping attribute with null value " + attr.Key ?? string.Empty;
                }
                else if (attr.Value.GetType().IsPrimitive || attr.Value is String || attr.Value is Guid || attr.Value is decimal)
                {
                    origRecord.Attributes.Add(attr.Key, attr.Value);
                }
                else if (attr.Value is EntityReference)
                {
                    var er = attr.Value as EntityReference;
                    origRecord.Attributes.Add(attr.Key, new EntityReference(er.LogicalName, er.Id));
                }
                else if (attr.Value is OptionSetValue)
                {
                    var v = (OptionSetValue)attr.Value;
                    origRecord.Attributes.Add(attr.Key, new OptionSetValue(v.Value));
                }
                else if (attr.Value is DateTime)
                {
                    var copy = (DateTime)attr.Value;
                    origRecord.Attributes.Add(attr.Key, copy);
                }
                else
                {
                    debugInfo += "Unknown type all changes will be cosidered as changes " + attr.Value.GetType();
                }
                // Note: if some other types are encountered, any changes to those attributes will be considered as changes         
            });

            debugInfo += "Orig record has fields: " + String.Join(",", origRecord.Attributes.Select(a => a.Key));

            record.PropertyChanged += delegate (object sender, System.ComponentModel.PropertyChangedEventArgs ev)
            {
                // We need use reflection here to figure out the attribute name for the property :/
                //AttributeLogicalNameAttribute nameAttr =
                //    (AttributeLogicalNameAttribute)record.GetType().GetProperty(ev.PropertyName).GetCustomAttribute(
                //    typeof(AttributeLogicalNameAttribute));
                //var attributeLogicalName = nameAttr.LogicalName;

                // This might blow up at some point but in sandbox reflection is not allowed
                var attributeLogicalName = ev.PropertyName.ToLower();

                // Nulling not allowed 
                if (record[attributeLogicalName] == null) return;

                if (!(origRecord.Contains(attributeLogicalName)))
                {
                    // empty string is stored as null in crm, so we don't treat whitespace as a change when the value is already null
                    if (!(record[attributeLogicalName] is string && String.IsNullOrWhiteSpace(record[attributeLogicalName] as string)))
                    {
                        hasChanges = true;
                        RecordWithOnlyChanges[attributeLogicalName] = record[attributeLogicalName];
                    }
                }
                else
                {
                    var origValue = origRecord[attributeLogicalName];
                    var newValue = record[attributeLogicalName];
                    var valChanged = false;
                    if (origValue.GetType().IsPrimitive || origValue is OptionSetValue || origValue is String || origValue is decimal)
                    {
                        valChanged = !origRecord[attributeLogicalName].Equals(newValue);
                    }
                    else if (origValue is DateTime)
                    {
                        var odt = (DateTime)origValue;
                        var ndt = (DateTime)record[attributeLogicalName];
                        // F**king crm date times
                        valChanged = Math.Abs(odt.Subtract(ndt).TotalHours) > 6;
                    }
                    else if (origValue is EntityReference)
                    {
                        valChanged = (origValue as EntityReference).Id != (newValue as EntityReference)?.Id;
                    }
                    else
                    {
                        debugInfo += "Unknown type -> marked as change: " + origValue.GetType().Name;
                        valChanged = true;
                    }

                    if (valChanged)
                    {
                        hasChanges = true;
                        debugInfo += $"Changing value of {attributeLogicalName} to {newValue} from {origValue}";
                        RecordWithOnlyChanges.Attributes[attributeLogicalName] = newValue;
                    }
                }
            };

        }

        public void Dispose()
        {

        }
    }
}
