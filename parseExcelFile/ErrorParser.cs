﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace parseExcelFile
{
    public class ErrorParser
    {
        public static string Returnerror (Exception e)
        {
            var result = StackTraceParser.Parse(
                e.StackTrace,
                (f, t, m, pl, ps, fn, ln) => new
                {
                    Line = ln,
                });
            string line = result.SingleOrDefault().Line;
            return Error(line);
        }
        internal static string Error(string line)
        {
            switch (line)
            {
                case "62":
                    return "Päivämäärä virheellinen (expenseBookingdate)";
                case "63":
                    return "Kululaji virheellinen (expenseExpensetype)";
                case "64":
                    return "Netto virheellinen (expenseNetto)";
                case "66":
                    return "Projekti virheellinen (expenseProject)";
                case "67":
                    return "Kululaji virheellinen (expenseExpensetype)";
                case "68":
                    return "Kuluselitte virheellinen (expenseInfo)";
                case "69":
                    return "Matkalaskun tila virheellinen (expenseCategory)";
               default:
                    return "Määrittelemätön virhe";
            }
        }
    }
}
